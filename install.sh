#!/usr/bin/env bash

# instalación paquetes necesarios
sudo apt-get install -y curl git wget zsh

# instalación ohmyzsh
if [ ! -d ~/.oh-my-zsh/.git ]; then
    #instalar ohmyzsh
    echo "instalar oh-my-zsh"
fi

NEOAUTO_LOCAL_HOST="172.18.60.57"

LOCAL_DIR_BARES=~/bares
HTDOCS_DIR=~/htdocs
NEOAUTO_BASE_DIR=$HTDOCS_DIR/apps/neoauto
ADECSYS_BASE_DIR=$HTDOCS_DIR/apps/adecsys

mkdir -p $LOCAL_DIR_BARES
mkdir -p $HTDOCS_DIR/apps
mkdir -p $HTDOCS_DIR/logs


#======== NEOAUTO ========
#web
if [ ! -d $NEOAUTO_BASE_DIR/neoauto3.com/.git ]; then
    mkdir -p $NEOAUTO_BASE_DIR/neoauto3.com
    cd $NEOAUTO_BASE_DIR/neoauto3.com
    git config core.fileMode false
    git fetch
    git checkout -f pre2d
fi

#admin
if [ ! -d $NEOAUTO_BASE_DIR/admin.neoauto3.com/.git ]; then
    mkdir -p $NEOAUTO_BASE_DIR/admin.neoauto3.com
    cd $NEOAUTO_BASE_DIR/admin.neoauto3.com
    git clone -b pre2d git@bitbucket.org:orbisunt/neoauto-admin.git .
fi

#resizer
if [ ! -d $NEOAUTO_BASE_DIR/resizer.neoauto3.com/.git ]; then
    mkdir -p $NEOAUTO_BASE_DIR/resizer.neoauto3.com
    cd $NEOAUTO_BASE_DIR/resizer.neoauto3.com
    git clone -b pre2d git@bitbucket.org:orbisunt/neoauto-resizer.git .
fi

#adecsys
if [ ! -d $ADECSYS_BASE_DIR/adecsys.orbis.pe/.git ]; then
    mkdir -p $ADECSYS_BASE_DIR/adecsys.orbis.pe
    cd $ADECSYS_BASE_DIR/adecsys.orbis.pe
    git clone -b pre1a git@bitbucket.org:orbisunt/adecsys-api.git .
fi




#composer build
docker build -t composer_img $HTDOCS_DIR/docker/composer




#web - directorios básicos
mkdir -p $NEOAUTO_BASE_DIR/neoauto3.com/public/tmp
chmod -R 0777 $NEOAUTO_BASE_DIR/neoauto3.com/public/tmp

mkdir -p $NEOAUTO_BASE_DIR/neoauto3.com/var/log
chmod -R 0777 $NEOAUTO_BASE_DIR/neoauto3.com/var

#web - directorios para composer
mkdir -p $NEOAUTO_BASE_DIR/neoauto3.com/vendor
mkdir -p $NEOAUTO_BASE_DIR/neoauto3.com/public/neoauto3/application/log
chmod -R 0777 $NEOAUTO_BASE_DIR/neoauto3.com/public/neoauto3/application/log
mkdir -p $NEOAUTO_BASE_DIR/neoauto3.com/public/neoauto3/vendor

#web - composer neo2
cd $NEOAUTO_BASE_DIR/neoauto3.com
docker run --rm -e DEV_UID=$(id -u) -e DEV_GID=$(id -g) -it -v $NEOAUTO_BASE_DIR/neoauto3.com:/base composer_img composer update --ignore-platform-reqs

#web - composer neo3
cd $NEOAUTO_BASE_DIR/neoauto3.com/public/neoauto3
docker run --rm -e DEV_UID=$(id -u) -e DEV_GID=$(id -g) -it -v $NEOAUTO_BASE_DIR/neoauto3.com/public/neoauto3:/base composer_img composer update --ignore-platform-reqs




#admin - directorios para composer
mkdir -p $NEOAUTO_BASE_DIR/admin.neoauto3.com/var/log
chmod -R 0777 $NEOAUTO_BASE_DIR/admin.neoauto3.com/var

#admin - composer
cd $NEOAUTO_BASE_DIR/admin.neoauto3.com
docker run --rm -e DEV_UID=$(id -u) -e DEV_GID=$(id -g) -it -v $NEOAUTO_BASE_DIR/admin.neoauto3.com:/base composer_img composer update --ignore-platform-reqs




#web - directorios básicos
mkdir -p $NEOAUTO_BASE_DIR/resizer.neoauto3.com/log
chmod -R 0777 $NEOAUTO_BASE_DIR/resizer.neoauto3.com/log

#resizer - directorios para composer
mkdir -p $NEOAUTO_BASE_DIR/resizer.neoauto3.com/vendor

#resizer - composer
cd $NEOAUTO_BASE_DIR/resizer.neoauto3.com
docker run --rm -e DEV_UID=$(id -u) -e DEV_GID=$(id -g) -it -v $NEOAUTO_BASE_DIR/resizer.neoauto3.com:/base composer_img composer update --ignore-platform-reqs




#adecsys - directorios para composer
mkdir -p $ADECSYS_BASE_DIR/adecsys.orbis.pe/vendor

#adecsys - composer
cd $ADECSYS_BASE_DIR/adecsys.orbis.pe
docker run --rm -e DEV_UID=$(id -u) -e DEV_GID=$(id -g) -it -v $ADECSYS_BASE_DIR/adecsys.orbis.pe:/base composer_img composer update --ignore-platform-reqs




sudo -- sh -c -e "grep -q -F '127.0.0.1    local.neoauto3.com' /etc/hosts || echo '127.0.0.1    local.neoauto3.com' >> /etc/hosts"
sudo -- sh -c -e "grep -q -F '127.0.0.1    local.admin.neoauto3.com' /etc/hosts || echo '127.0.0.1    local.admin.neoauto3.com' >> /etc/hosts"
sudo -- sh -c -e "grep -q -F '127.0.0.1    local.resizer.neoauto3.com' /etc/hosts || echo '127.0.0.1    local.resizer.neoauto3.com' >> /etc/hosts"
sudo -- sh -c -e "grep -q -F '127.0.0.1    local.adecsys.orbis.pe' /etc/hosts || echo '127.0.0.1    local.adecsys.orbis.pe' >> /etc/hosts"



cd $HTDOCS_DIR
docker-compose build
docker-compose up -d
